pragma solidity ^0.8.7;

import "./interfaces/IRng.sol";
import "./interfaces/INFT.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";

contract Slot1 is Initializable,OwnableUpgradeable {

    address public rng;
    address public cardDeck;
    address public rdt;

    bool public paused;

    uint256 public maxRandomInt;
    uint256 comboCount;

    uint256[] public probabilityArray; 

    mapping(uint256 => uint256[]) public bracketCombos;
    mapping(uint256 => uint256) public jackPotNum; // the rng number at which jackPot is won at each probability
    mapping(uint256 => uint256) public percentAmounts; // the amount of tokens to be won for different % of probablility (50% - 100RDT,10%)
    mapping(address=>uint256) public rngReqIds;
    mapping(uint256 => uint256) public slotCalls;

    function init(address _rng,address _deck,uint256 _maxRngInt,address _rdt) external initializer {
        rng = _rng;
        cardDeck = _deck;
        maxRandomInt = _maxRngInt;
        rdt = _rdt;
        jackPotNum[5000] = 1;
        jackPotNum[2000] = 77;
        jackPotNum[3000] = 777;
        jackPotNum[50] = 47;

    }

    function addCombo(uint256[] memory _combo) external onlyOwner {
        comboCount++;
        bracketCombos[comboCount] = _combo;
    }

    function addPrizeWeight() external onlyOwner {
        //TODO
    }


    function checkEligibility(address _user,uint256 bracket) public view returns (bool){
        bool finalBool;
        uint256[] memory x=bracketCombos[bracket];
        for(uint256 i=0;i<x.length;i++){
            if(INFT(cardDeck).balanceOf(_user,x[i])>=1){
                finalBool = true;
            }
            else {
                return false;
            }
        }
        return finalBool;
    }

    function callSlot(uint256 bracket) external {
        require(checkEligibility(msg.sender, bracket),"Not enough nft combos");
        uint256[] memory y = bracketCombos[bracket];
        for(uint256 i=0;i<y.length;i++){
            INFT(cardDeck).burn(msg.sender,y[i],1);
        }
        require(slotCalls[rngReqIds[msg.sender]] ==0);
        rngReqIds[msg.sender] = IRNG(rng).requestRandomWords(bracket);

    }

    function viewReward(address _user) public view returns (uint256[] memory r) {
        uint256 result = IRNG(rng).viewRandomResult(rngReqIds[_user]) % 10000;
        uint256[] memory prize;
        uint256[] memory prob = probabilityArray;
        if(result ==0){
            return new uint256[](prob.length);
        }
        for(uint256 i=0;i<prob.length;i++){
            if(result % 100000/prob[i]==jackPotNum[prob[i]]){
                prize[i] = percentAmounts[prob[i]];
            }
        }
        /*
        if(result ==0){
            return [uint256(0),uint256(0),uint256(0),uint256(0)];
        }
        if(result % 2 ==0){
            prize[0] =percentAmounts[500];
        }
        if(result % 50 == jackPotNum[50]){
            prize[1] = percentAmounts[20];
        }
        if(result % 100 == jackPotNum[2]){
            prize[2] = percentAmounts[100];
        }
        if(result % 1000 == jackPotNum[3]){
            prize[3] = percentAmounts[1000];
        }
        */
        return prize;
    }

    function reveal() external {
        uint256 reqId = rngReqIds[msg.sender];
        require(reqId !=0);
        require(IRNG(rng).viewRandomResult(reqId)!=0);
        uint256[] memory r = viewReward(msg.sender);
        for(uint256 i=0;i<r.length;i++){
            if(r[i]>0){
                IERC20Upgradeable(rdt).transfer(msg.sender,r[i]);
            }
        }
        delete rngReqIds[msg.sender];
    }
