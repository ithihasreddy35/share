// Decompiled by library.dedaub.com
// 2022.11.15 07:51 UTC

// Data structures and variables inferred from the use of storage instructions
mapping (uint256 => [uint256]) map_2; // STORAGE[0x2]
uint256 stor_0_0_19; // STORAGE[0x0] bytes 0 to 19
uint256 _rawFulfillRandomWords; // STORAGE[0x1] bytes 0 to 19


function () public payable { 
    revert();
}

function rawFulfillRandomWords(uint256 varg0, uint256[] varg1) public nonPayable { 
    require(msg.data.length - 4 >= 64);
    require(varg1 <= 0xffffffffffffffff);
    require(4 + varg1 + 31 < msg.data.length);
    require(varg1.length <= 0xffffffffffffffff, Panic(65));
    require(!((MEM[64] + ((varg1.length << 5) + 63 & 0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe0) > 0xffffffffffffffff) | (MEM[64] + ((varg1.length << 5) + 63 & 0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe0) < MEM[64])), Panic(65));
    MEM[64] = MEM[64] + ((varg1.length << 5) + 63 & 0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe0);
    v0 = v1 = MEM[64] + 32;
    require(32 + ((varg1.length << 5) + (4 + varg1)) <= msg.data.length);
    v2 = v3 = 36 + varg1;
    while (v2 < 32 + ((varg1.length << 5) + (4 + varg1))) {
        MEM[v0] = msg.data[v2];
        v2 += 32;
        v0 += 32;
    }
    require(msg.sender == _rawFulfillRandomWords, OnlyCoordinatorCanFulfill(msg.sender, _rawFulfillRandomWords));
    v4 = v5 = 0;
    require(uint32(map_2[varg0][5]) <= 0xffffffffffffffff, Panic(65));
    v6 = new uint256[](uint32(map_2[varg0][5]));
    if (uint32(map_2[varg0][5])) {
        CALLDATACOPY(v6.data, msg.data.length, uint32(map_2[varg0][5]) << 5);
    }
    require(uint32(map_2[varg0][5]) <= 0xffffffffffffffff, Panic(65));
    v7 = new uint256[](uint32(map_2[varg0][5]));
    if (uint32(map_2[varg0][5])) {
        CALLDATACOPY(v7.data, msg.data.length, uint32(map_2[varg0][5]) << 5);
    }
    v8 = v9 = 0;
    while (uint32(v8) < uint32(map_2[varg0][5])) {
        if (v4 < map_2[varg0][3]) {
            break;
        }
        if (v4 >= map_2[varg0][3]) {
            break;
        }
        if (map_2[varg0][4] != 0x8000000000000000000000000000000000000000000000000000000000000000) {
            break;
        }
        require(map_2[varg0][4] != 0x8000000000000000000000000000000000000000000000000000000000000000, Panic(17));
        if (v4 > 0 - map_2[varg0][4]) {
            break;
        }
        if (v4 <= 0 - map_2[varg0][4]) {
            break;
        }
        if (uint32(v8) < varg1.length) {
            break;
        }
        require(uint32(v8) < varg1.length, Panic(50));
        if (2) {
            break;
        }
        require(2, Panic(18));
        if (uint32(v8) < v6.length) {
            break;
        }
        require(uint32(v8) < v6.length, Panic(50));
        v6[uint32(v8)] = 0xff & (0xff & MEM[32 + (uint32(v8) << 5) + MEM[64]] % 2);
        if (uint32(v8) < v6.length) {
            break;
        }
        require(uint32(v8) < v6.length, Panic(50));
        v10 = v11 = 1 == 0xff & v6[uint32(v8)];
        if (v11) {
            v10 = 1 == 0xff & map_2[varg0][5] >> 32;
        }
        if (!v10) {
            require(uint32(v8) < v6.length, Panic(50));
            v12 = v13 = 0 == 0xff & v6[uint32(v8)];
            if (v13) {
                v12 = !(0xff & map_2[varg0][5] >> 32);
            }
            if (!v12) {
                require(!((map_2[varg0][2] >= 0) & (v4 < 0x8000000000000000000000000000000000000000000000000000000000000000 + map_2[varg0][2])), Panic(17));
                require(!((map_2[varg0][2] < 0) & (v4 > 0x7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff + map_2[varg0][2])), Panic(17));
                v4 = v4 - map_2[varg0][2];
            } else {
                v14 = v15 = 10000;
                v16 = v17 = _SafeMul(9800, map_2[varg0][2]);
            }
        } else {
            v14 = v18 = 10000;
            v16 = v19 = _SafeMul(9800, map_2[varg0][2]);
        }
        v20 = _SafeDiv(v16, v14);
        require(!((v4 >= 0) & (v20 > 0x7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff - v4)), Panic(17));
        require(!((v4 < 0) & (v20 < 0x8000000000000000000000000000000000000000000000000000000000000000 - v4)), Panic(17));
        v4 = v4 + v20;
        v21 = _SafeMul(19800, map_2[varg0][2]);
        v22 = _SafeDiv(v21, 10000);
        v4 = _SafeAdd(v4, v22);
        v23 = _SafeMul(19800, map_2[varg0][2]);
        v24 = _SafeDiv(v23, 10000);
        require(uint32(v8) < v7.length, Panic(50));
        v7[uint32(v8)] = v24;
        require(uint32(v8) != 0xffffffff, Panic(17));
        v8 = 1 + uint32(v8);
    }
    require(uint32(map_2[varg0][5]) >= uint32(v8), Panic(17));
    v25 = _SafeMul(uint32(uint32(uint32(map_2[varg0][5])) - uint32(v8)), map_2[varg0][2]);
    v26 = _SafeAdd(v4, v25);
    v27 = new array[](v6.length);
    v28 = v29 = v27.data;
    v30 = v31 = v6.data;
    v32 = v33 = 0;
    while (v32 < v6.length) {
        MEM[v28] = 0xff & MEM[v30];
        v30 += 32;
        v28 += 32;
        v32 += 1;
    }
    v28 = new array[](v7.length);
    v34 = v35 = v28.data;
    v36 = v37 = v7.data;
    v38 = v39 = 0;
    while (v38 < v7.length) {
        MEM[v34] = MEM[v36];
        v34 += 32;
        v36 += 32;
        v38 += 1;
    }
    emit 0x63ba2c91a70f945b84c24531b0de813d66f430987169cb7d878431c04cb0004(address(map_2[varg0]), map_2[varg0][2], v26, address(map_2[varg0][0x1]), v27, v28, uint32(v8));
    require(stor_0_0_19.code.size);
    v40 = stor_0_0_19.call(0x6c025ec200000000000000000000000000000000000000000000000000000000, address(map_2[varg0]), v26, address(map_2[varg0][0x1])).gas(msg.gas);
    require(v40); // checks call status, propagates error data on error
    map_2[varg0] = 0xffffffffffffffffffffffff0000000000000000000000000000000000000000 & map_2[varg0];
    map_2[varg0][1] = 0xffffffffffffffffffffffff0000000000000000000000000000000000000000 & map_2[varg0][0x1];
    map_2[varg0][2] = 0;
    map_2[varg0][3] = 0;
    map_2[varg0][4] = 0;
    map_2[varg0][5] = 0xffffffffffffffffffffffffffffffffffffffffffffffffffffff0000000000 & map_2[varg0][0x5];
}

function 0x25425363() public nonPayable { 
    return stor_0_0_19;
}

function 0x6d974773(uint256 varg0, uint256 varg1, uint256 varg2, uint256 varg3, uint256 varg4, uint256 varg5) public payable { 
    require(msg.data.length - 4 >= 192);
    require(varg1 == address(varg1));
    require(varg2 == varg2);
    require(varg3 == uint32(varg3));
    v0, v1 = stor_0_0_19.staticcall(0x298729d800000000000000000000000000000000000000000000000000000000, this).gas(msg.gas);
    require(v0); // checks call status, propagates error data on error
    require(MEM[64] + RETURNDATASIZE() - MEM[64] >= 32);
    require(v1 == v1);
    require(v1, Error('not valid'));
    v2 = v3 = uint32(varg3) > 0;
    if (v3) {
        v2 = uint32(varg3) < 500;
    }
    require(v2, Error('invalid bet number'));
    if (address(varg1)) {
        v4 = _SafeMul(varg0, uint32(varg3));
        require(stor_0_0_19.code.size);
        v5 = stor_0_0_19.call(0xbfd8b5a400000000000000000000000000000000000000000000000000000000, msg.sender, v4, address(varg1)).gas(msg.gas);
        require(v5); // checks call status, propagates error data on error
        goto 0x18d;
    } else {
        v6 = _SafeMul(varg0, uint32(varg3));
        require(msg.value == v6);
        v7, v8 = stor_0_0_19.call().value(msg.value).gas(msg.gas);
        if (RETURNDATASIZE() != 0) {
            v9 = new bytes[](RETURNDATASIZE());
            RETURNDATACOPY(v9.data, 0, RETURNDATASIZE());
        }
        require(v7, Error('eth transfer failed'));
    }
    v10, v11 = _rawFulfillRandomWords.requestRandomWords(0x4b09e658ed251bcafeebbc69400383d49f344ace09b9576fe248bb02c003fe9f, 1210, 3, 0x2625a0, uint32(varg3)).gas(msg.gas);
    require(v10); // checks call status, propagates error data on error
    require(MEM[64] + RETURNDATASIZE() - MEM[64] >= 32);
    map_2[v11] = 0xffffffffffffffffffffffff0000000000000000000000000000000000000000 & map_2[v11] | msg.sender;
    map_2[v11][1] = 0xffffffffffffffffffffffff0000000000000000000000000000000000000000 & map_2[v11][0x1] | address(varg1);
    map_2[v11][2] = varg0;
    map_2[v11][3] = varg4;
    map_2[v11][4] = varg5;
    map_2[v11][5] = uint32(varg3) | map_2[v11][0x5] & 0xffffffffffffffffffffffffffffffffffffffffffffffffffffff0000000000 | varg2 << 32;
}

function 0x953995ea() public nonPayable { 
    return _rawFulfillRandomWords;
}

function _SafeMul(uint256 varg0, uint256 varg1) private { 
    require(!(varg0 & (varg1 > 0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff / varg0)), Panic(17));
    return varg0 * varg1;
}

function _SafeDiv(uint256 varg0, uint256 varg1) private { 
    require(varg1, Panic(18));
    return varg0 / varg1;
}

function _SafeAdd(uint256 varg0, uint256 varg1) private { 
    require(varg0 <= ~varg1, Panic(17));
    return varg0 + varg1;
}

// Note: The function selector is not present in the original solidity code.
// However, we display it for the sake of completeness.

function __function_selector__(bytes4 function_selector) public payable { 
    MEM[64] = 128;
    if (msg.data.length >= 4) {
        if (0x1fe543e3 == function_selector >> 224) {
            rawFulfillRandomWords(uint256,uint256[]);
        } else if (0x25425363 == function_selector >> 224) {
            0x25425363();
        } else if (0x6d974773 == function_selector >> 224) {
            0x6d974773();
        } else if (0x953995ea == function_selector >> 224) {
            0x953995ea();
        }
    }
    ();
}
